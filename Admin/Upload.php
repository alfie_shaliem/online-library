<?php

        session_start(); 

		$level = $_SESSION['level'];
if($level!=1){
	header('location:../Index.php');
}
	
$username = $_SESSION['username'];

if(empty($_SESSION['username'])){
	header('location:../Index.php');
}

?>

<!DOCTYPE HTML>
<html>

	<!-----HEADER------>

<head>
	<link href="../Config/Template.css" type="text/css" rel="stylesheet">
	<link href="../Config/Dropdown.css" type="text/css" rel="stylesheet">
	<link href="../Config/Tamu.css" type="text/css" rel="stylesheet">
	<title>Home</title>

</head>
<body>
		<div class="header">
		<table class="header">
		
		<tr>
			<td>
				<img class="logo" src="../Images/logo-baznas.png">
			</td>
			
			<td>
				<h1>ONLINE LIBRARY</h1>
			</td>
			
			<td class="search">
			
			<form>
				<input class="search" type="text" placeholder="Search Pdf.." name="search">
				<input class="searchbutton" type="button" value="Search">	
			</form>	
				
			</td>
			
			</tr>
		
		</table>
		</div>
		
		<div class="menu-wrap">
	<ul>
		
		<li><a href="Home.php">Home</a></li>
		<li><a href="../Library.php">Library</a></li>
		
		<li class="Drop2"><a>Admin</a>
			<ul>
				<li><a href="#">Upload PDF</a></li>
				<li><a href="../Signup.php">Add User</a></li>
				<li><a href="Edit-pdf.php">Edit</a></li>
			</ul>
		</li>
		<li class="Drop"><a><img class="Drop" src="../Images/Dropdown.png"></a>
			<ul>
				<li><a><?php echo $username; ?></a></li>
				<li><a href="../Logout.php">Log Out</a></li>
			</ul>
		</li>
		
	</ul>
		</div>
		
	<!-----CLOSE HEADER------>	
	
	
	
			<!-----BODY------->	
	<div class="chest">
	
	<div class="form">
	
	<h3>File Upload Form</h3>
	<br><br>
	<form method="post" action="Upload.php" enctype="multipart/form-data">
	<table class="form" class="upload" style="font-family:calibri">
	<tr><th>Title</th> <td><input type="text" name="newname">&nbsp; .PDF</td></tr>
	
	<tr><th>Type</th> <td>
	
	<select name="tipe">
		
		<option value="Peraturan">Peraturan</option>
		
		<option value="Pedoman">Pedoman</option>
	
	</select>
	</td></tr>
	
	<tr><th>Content</th> <td><input type="file" accept="application/pdf" name="userfile"></td></tr>
	
	<tr><th>Description</th> <td><textarea name="description" rows=4 cols=30 maxlength="160"></textarea></td></tr>
	
	<tr><td colspan=2><input type="submit" value="Upload" name="upload"></td></tr>
	</table>
	</form>
	</div>
	
	</div>
			<!-----CLOSE BODY------>
			
			
	<!-----Footer------>
	<div class="footer">
	<div class="footer2">
	<p class="copyright">Copyright @ <?php echo date('Y'); ?> Online - Library by Asharisan. Alrights Reserved.
	</p>
	</div>
	<ul>
		
		<li><a href="Home.php">Home</a></li>
		<li><a href="About.php">About Us</a></li>
		<li><a>Library</a>
			
		</li>
		<li><a>Address</a>
			
		</li>
		
		
	</ul>
	</div>
<html>

<?php
include_once '../Config/Koneksi.php';


if(isset($_POST['upload']) && $_FILES['userfile']['size'] > 0)
{
$pdf = ".pdf";	
$newname  =	$_POST['newname'].$pdf;
$description  =	$_POST['description'];
$tipe  =	$_POST['tipe'];
	
$fileName = $_FILES['userfile']['name'];
$tmpName  = $_FILES['userfile']['tmp_name'];
$fileSize = $_FILES['userfile']['size'];
$fileType = $_FILES['userfile']['type'];

$tgl = date('Y-m-d');

$allowedExts = array(
  "pdf", 
  "doc", 
  "docx"
); 

$allowedMimeTypes = array( 
  'application/msword',
  'text/pdf',
  'image/gif',
  'image/jpeg',
  'image/png'
);

$extension = end(explode(".", $newname));

if ( 3000000 < $fileSize  ) {
  die( 'Please provide a smaller file [E/1].' );
}

if ( ! ( in_array($extension, $allowedExts ) ) ) {
  die('Please provide another file type [E/2].');
}



if(!get_magic_quotes_gpc())
{
    $newname = addslashes($newname);
}




$uploads_dir = "../Uploads";

 
move_uploaded_file($tmpName, "$uploads_dir/$newname");




$query2 = "INSERT INTO tb_document (nama_file, size, type, tgl_upload, username, deskripsi, tipe ) 
VALUES ('$newname', '$fileSize', '$fileType', '$tgl', '$username','$description', '$tipe')";

$hasil = mysqli_query($konek,$query2) or die('Error, query failed'); 

if($hasil){
echo "<br>File $newname uploaded <br>";
}
} 
?>




