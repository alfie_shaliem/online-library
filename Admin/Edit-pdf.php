
<?php

        session_start(); 

$level = $_SESSION['level'];
if($level!=1){
	header('location:../Index.php');
}
	
$username = $_SESSION['username'];

if(empty($_SESSION['username'])){
	header('location:../Index.php');
}

?>

<!DOCTYPE HTML>
<html>

	<!-----HEADER------>

<head>
	<link href="../Config/Template.css" type="text/css" rel="stylesheet">
	<link href="../Config/Dropdown.css" type="text/css" rel="stylesheet">
	<link href="../Config/Library.css" rel="Stylesheet" type="text/css">
	<link href="../Config/Paging.css" rel="Stylesheet" type="text/css">
	<title>Home</title>

</head>
<body>
		<div class="header">
		<table class="header">
		
		<tr>
			<td>
				<img class="logo" src="../Images/logo-baznas.png">
			</td>
			
			<td>
				<h1>ONLINE LIBRARY</h1>
			</td>
			
			<td class="search">
			
			<form>
				<input class="search" type="text" placeholder="Search.." name="search">
				<input class="searchbutton" type="button" value="Search">	
			</form>	
				
			</td>
			
			</tr>
		
		</table>
		</div>
		
		<div class="menu-wrap">
	<ul>
		
		<li><a href="Home.php">Home</a></li>
		<li><a href="../Library.php">Library</a></li>
		
		<li class="Drop2"><a>Admin</a>
			<ul>
				<li><a href="Upload.php">Upload PDF</a></li>
				<li><a href="../Signup.php">Add User</a></li>
				<li><a href="#">Edit</a></li>
			</ul>
		</li>
		<li class="Drop"><a><img class="Drop" src="../Images/Dropdown.png"></a>
			<ul>
				<li><a href="<?php echo "Profile.php?".$username."";    ?>"><?php echo $username; ?></a></li>
				<li><a href="../Logout.php">Log Out</a></li>
			</ul>
		</li>
		
	</ul>
		</div>
		
	<!-----CLOSE HEADER------>	
	
	
	
			<!-----BODY------->	
	<div class="chest">
	
	
<?php echo isi(); ?>
	
	
	</div>
			<!-----CLOSE BODY------>
			
			
	<!-----Footer------>
	<div class="footer">
	<div class="footer2">
	<p class="copyright">Copyright @ <?php echo date('Y'); ?> Online - Library by Asharisan. Alrights Reserved.
	</p>
	</div>
	<ul>
		
		<li><a href="Home.php">Home</a></li>
		<li><a href="About.php">About Us</a></li>
		<li><a>Library</a>
			
		</li>
		<li><a>Address</a>
			
		</li>
		
		
	</ul>
	</div>
<html>
<?php







function isi() {
include_once "../Config/Koneksi.php";


function byteFormat($bytes, $unit = "", $decimals = 2) {
 $units = array('B' => 0, 'KB' => 1, 'MB' => 2, 'GB' => 3, 'TB' => 4, 
 'PB' => 5, 'EB' => 6, 'ZB' => 7, 'YB' => 8);
 
 $value = 0;
 if ($bytes > 0) {
 // Generate automatic prefix by bytes 
 // If wrong prefix given
 if (!array_key_exists($unit, $units)) {
 $pow = floor(log($bytes)/log(1024));
 $unit = array_search($pow, $units);
 }
 
 // Calculate byte value by prefix
 $value = ($bytes/pow(1024,floor($units[$unit])));
 }
 
 // If decimals is not numeric or decimals is less than 0 
 // then set default value
 if (!is_numeric($decimals) || $decimals < 0) {
 $decimals = 2;
 }
 
 // Format output
 return sprintf('%.' . $decimals . 'f '.$unit, $value);
  }

$batas = 7;
$halaman = @$_GET['halaman'];
if(empty($halaman)){
$posisi = 0;
$halaman = 1;
}
else {
$posisi = ($halaman-1) * $batas;
}

$query1 = "select * from tb_document  order by nama_file LIMIT $posisi, $batas";

$hasil = mysqli_query($konek,$query1);


echo "<table >
<tr><th>No.</th><th>&nbsp;</th><th>Title</th></tr>
<tr><td colspan=\"6\"><hr></td></tr>
";

$no =$posisi + 1;

while($data = mysqli_fetch_array($hasil))
{
	if (strlen($data['deskripsi']) > 105) {
		
$trimstring = substr($data['deskripsi'], 0, 100). ' <a href="Readmore-pdf.php?id='.$data['id'].'">readmore...</a>';
} else {
$trimstring = $data['deskripsi'];
}
	
	
	echo "	<tr>
				<td rowspan=\"4\">$no</td>
				<td rowspan=\"4\" class=\"icon\">
					<img src=\"../Images/pdf.png\" height=\"60px\" width=\"60px\">
				</td>
				<th rowspan=\"2\" colspan=\"2\">
					$data[nama_file]
				</th>
				<td>$data[tipe]</td>
				<td rowspan=\"2\">
					
				</td>
			</tr>
			
			<tr>
			</tr>
			
			<tr>
				<td rowspan=2  class=\"title\">
					".$trimstring."
				</td>
				<td>
				Admin <td>: $data[username]</td>
				</td>
				<td rowspan=\"2\">
					<form method=\"post\" action=\"Edit-Edit-pdf.php?id=$data[id]\">
					
						<input type=\"hidden\" name=\"filename\" value=\"".$data['nama_file']."\">
						<button type=\"submit\" name=\"edit\">Edit</button>
					
					</form>
				
					<form method=\"get\" action=\"Edit-Delete-pdf.php\">
					
						<input type=\"hidden\" name=\"filename\" value=\"".$data['nama_file']."\">
						<button type=\"submit\" name=\"delete\">Delete</button>
					
					</form>
					
				</td>
				
			</tr>
			<tr>
				
				<td>
				Size <td> :&nbsp; ".byteFormat($data['size'],'KB','0')."</td>
				</td>
				
			</tr>
			<tr><td colspan=6><hr></td></tr>
			";
$no++;

}


echo "</table>";

$tampil2 = mysqli_query($konek, "select * from tb_document");
$jmldata = mysqli_num_rows($tampil2);
$jmlhalaman =ceil($jmldata/$batas);

$file = $_SERVER['PHP_SELF'];

echo "<div class=\"paging\">";

if($halaman > 1){
	
$prev=$halaman-1;

echo "<span class=\"prevnext\">

<a href=\"$file?halaman=$prev\"><< Prev</a>

</span> ";


}

else{
echo "<span class=disabled><< Prev</span> ";
}

for($i=1;$i<=$jmlhalaman;$i++)
if ($i != $halaman) {
echo "<a href=\"$file?halaman=$i\">$i</a> ";
}
else{
echo "<span class=\"current\">$i</span> ";
}

if($halaman < $jmlhalaman){
$next=$halaman+1;
echo "<span class=\"prevnext\">
<a href=\"$file?halaman=$next\">Next >></a>
</span> ";
}
else{
echo "<span class=disabled>Next >></span> ";
}
echo "</div>";

echo "<br><p class=\"absolute\">Jumlah File: <b>$jmldata</b> File</p>";
}
?>